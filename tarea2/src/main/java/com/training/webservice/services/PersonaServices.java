package com.training.webservice.services;

import com.training.webservice.entity.Persona;
import com.training.webservice.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServices {

    @Autowired
    private PersonaRepository repo;

    public Persona guardarPersona(){
        Persona person;
        try {
            person = new Persona();
            person.setDocumento("0987654321");
            person.setNombres("JOSE JOSE");
            person.setApellidos("PEREZ PEREZ");
            person = repo.save(person);
            System.out.println("// persona: " + person);
        } catch (Exception e){
            System.out.println(e);
            person = new Persona();
            person.setNombres("NO SE PUDO REGISTRAR");
        }
        return person;
    }

    public Persona guardarPersona(String documento, String nombre, String apellidos){
        Persona person;
        try {
            person = new Persona();
            person.setDocumento(documento);
            person.setNombres(nombre);
            person.setApellidos(apellidos);
            person = repo.save(person);
            System.out.println("// persona: " + person);
        } catch (Exception e){
            System.out.println(e);
            person = new Persona();
            person.setNombres("NO SE PUDO REGISTRAR");
        }
        return person;
    }

}
