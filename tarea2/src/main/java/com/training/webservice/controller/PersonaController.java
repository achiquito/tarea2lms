package com.training.webservice.controller;

import com.training.webservice.entity.Persona;
import com.training.webservice.model.ClientModel;
import com.training.webservice.services.PersonaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tarea2/persona")
public class PersonaController {

    @Autowired
    private PersonaServices service;

    @GetMapping(path = "/registrar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Persona> saveData(@RequestParam(name = "documento") String documento,
                                            @RequestParam(name = "nombres") String nombres,
                                            @RequestParam(name = "apellidos") String apellidos) {
        Persona person = service.guardarPersona(documento, nombres, apellidos);
        return new ResponseEntity<>(person, HttpStatus.resolve(200));
    }

}
