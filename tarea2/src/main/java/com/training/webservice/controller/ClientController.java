package com.training.webservice.controller;

import com.training.webservice.model.ClientModel;
import com.training.webservice.services.ClientServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/clients")
public class ClientController {

    @Autowired
    ClientServices service;

    @GetMapping(path = "/query", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientModel> getData(@RequestParam(name = "documentType") String documentType,
                                               @RequestParam(name = "numberPhone") Integer numberPhone) {
        return service.getData(documentType, numberPhone);
    }

    @GetMapping(path = "/consulta", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientModel> getDataClient(@RequestParam(name = "tipoDocumento") String tipo,
                                               @RequestParam(name = "numeroDocumento") String numero) {
        return service.getDataCliente(tipo, numero);
    }

}
